<?php /* Template Name: Homepage */ ?>
<?php get_header(); ?>

<section id="banner-top" class="container-fluid">
	<div class="row">
		<div>
			<?php 
				if (wp_is_mobile()) {
					echo '<img src="'.get_template_directory_uri().'/assets/imgs/banner-top-mobile.jpg" alt="banner promocional superior">';
				} else {
					echo '<img src="'.get_template_directory_uri().'/assets/imgs/banner-top.jpg" alt="banner promocional superior">';
				};
			?>
		</div>
	</div>
</section>

<section id="descubre" class="container text-center mt-40 mb-40">
	<div class="row visible-xs">
		<div class="col-xs-12">
			<img style="max-width: 115px;" src="<?php echo get_template_directory_uri(); ?>/assets/imgs/logo-matte.png" alt="">
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<h1><strong>DESCUBRE LO NUEVO DE LEBEN</strong> EN PROVIDENCIA</h1>
		</div>
		<?php 
			if (wp_is_mobile()) {
				echo '
				<div class="col-xs-12 desde">
					[ Desde <b>UF 6.490</b> ]
				</div>
				';
			} else {
			};
		?>
		<div class="col-xs-12 txt-pasos">
			Ubicado a pasos de Barrio Italia y Metro Salvador, cuenta con 48 departamentos desde 62 m2 a 137 m2.
		</div>
		<div class="col-xs-12">
			<span class="top-line"><strong>2  ·  3</strong>  Dormitorios, Dúplex y Dúplex con Rooftop</span>
		</div>
	</div>
</section>

<section id="contacto" class="container p-100">
	<div class="row p-40 lblue">
		<div class="col-xs-12 col-sm-4 t-gold hidden-xs">
			<h2>INSCRÍBETE HOY</h2>
			<!--<p>Ricardo Matte 557,<br>Providencia.</p>-->
			<img class="mapa" src="<?php echo get_template_directory_uri(); ?>/assets/imgs/map.svg" alt="mapa referencial">
		</div>
		<h2 class="visible-xs">INSCRÍBETE HOY</h2>
		<div class="col-xs-12 col-sm-8 contact-form">
			<?php echo do_shortcode('[contact-form-7 id="6" title="Contacto landing"]')?>
		</div>
	</div>
	<div class="row text-center visible-xs">
		<img class="mapa" src="<?php echo get_template_directory_uri(); ?>/assets/imgs/map.svg" alt="mapa referencial">
	</div>
</section>

<section id="icons" class="container">
	<div class="row">
		<div class="col-xs-2 col-sm-2 icon">
			<img class="icon-img" src="<?php echo get_template_directory_uri(); ?>/assets/imgs/icon-metro.svg" alt="Mejor Conectividad">
			<p class="hidden-xs">Mejor Conectividad</p>
		</div>
		<div class="col-xs-2 col-sm-2 icon">
			<img class="icon-img" src="<?php echo get_template_directory_uri(); ?>/assets/imgs/icon-gym.svg" alt="Gym Training Zone">
			<p class="hidden-xs">Training Zone</p>
		</div>
		<div class="col-xs-2 col-sm-2 icon">
			<img class="icon-img" src="<?php echo get_template_directory_uri(); ?>/assets/imgs/icon-bici.svg" alt="Punto de reparación de bicicletas">
			<p class="hidden-xs">Punto de Reparación</p>
		</div>
		<div class="col-xs-2 col-sm-2 icon">
			<img class="icon-img" src="<?php echo get_template_directory_uri(); ?>/assets/imgs/icon-pet.svg" alt="Pet Zone">
			<p class="hidden-xs">Pet Zone</p>
		</div>
		<div class="col-xs-2 col-sm-2 icon">
			<img class="icon-img" src="<?php echo get_template_directory_uri(); ?>/assets/imgs/icon-lock.svg" alt="Ecommerce Lockers">
			<p class="hidden-xs">Ecommerce Lockers</p>
		</div>
		<div class="col-xs-2 col-sm-2 icon">
			<img class="icon-img" src="<?php echo get_template_directory_uri(); ?>/assets/imgs/icon-car.svg" alt="Carga Eléctrica para Autos">
			<p class="hidden-xs">Carga Eléctrica para Autos</p>
		</div>
	</div>
</section>

<section id="btn-maps-waze" class="container mb-40 hidden-xs">
	<div class="row">
		<div class="col-xs-12 text-center">
			<a href="https://www.google.cl/maps/place/Ricardo+Matte+P%C3%A9rez+557,+Providencia,+Regi%C3%B3n+Metropolitana/@-33.4390417,-70.6278597,17z/data=!3m1!4b1!4m5!3m4!1s0x9662c58109bde1b5:0x4f9231855eda4993!8m2!3d-33.4390462!4d-70.625671" target="_blank"><button id="btn-mw" class="btn g-btn">Ver ubicación en maps</button></a>
		</div>
	</div>
</section>

<section id="" class="container visible-xs p0">
	<div class="row p0">
		<div class="col-xs-12 text-center">
		<a href="https://www.waze.com/ul?ll=-33.43904620%2C-70.62567100&navigate=yes" target="_blank"></a><button id="btn-mw" class="btn g-btn">Ver ubicación en waze</button></a>
		</div>
	</div>
</section>



<?php get_footer() ?>