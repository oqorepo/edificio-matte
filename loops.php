<!-- LOOP PLANTAS --> 
<?php
	$loop = new WP_Query( array(
		'post_type' => 'plantas',
		'posts_per_page' => -1
		)
	);
?>
<?php if ( $loop->have_posts() ) :  ?>
	<script>
		var plantas = [];
		<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
		unaplanta = 
			{
				tipo: "<?php echo get_field('tipo_de_planta'); ?>",
				programa: "<?php echo get_field('programa'); ?>",
				valorUF: "<?php echo get_field('valoruf'); ?>",
				m2util: "<?php echo get_field('m2util'); ?>",
				ufm2renta: "<?php echo get_field('ufm2renta'); ?>",
				foto: "<?php echo get_field('imagen_planta');?>",
				stotal: "<?php echo get_field('superficie_total');?>",
				sinterior: "<?php echo get_field('superficie_interior');?>",
				sterraza: "<?php echo get_field('superficie_terraza');?>"	
			};
		plantas.push(unaplanta);
<?php endwhile; wp_reset_query(); ?>
</script>
<?php endif; ?>