<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-3">
				<img class="logo-bottom hidden-xs" src="<?php echo get_template_directory_uri(); ?>/assets/imgs/logo.svg" alt="logo proyecto">
			</div>
			<div class="col-xs-12 col-sm-9 text-right">
				<ul class="menu-bottom">
					<li><a href="https://www.google.cl/maps/place/Ricardo+Matte+P%C3%A9rez+557,+Providencia,+Regi%C3%B3n+Metropolitana/@-33.4390417,-70.6278597,17z/data=!3m1!4b1!4m5!3m4!1s0x9662c58109bde1b5:0x4f9231855eda4993!8m2!3d-33.4390462!4d-70.625671" target="_blank">Oficina central: Dr. Manuel Barros Borgoño 386, Providencia</a></li>
					<li class="hidden-xs"><a href="tel:+56226201600">+56 2 2620 1600</a></li>
					<li class="hidden-xs"><a href="mailto:matte@ileben.cl">matte@ileben.cl</a></li>
				</ul>
				<ul class="menu-bottom-2">
					<li class="visible-xs" style="border-right:1px solid white;"><a href="tel:+56226201600">+56 2 2620 1600</a></li>
					<li class="visible-xs"><a href="mailto:matte@ileben.cl">matte@ileben.cl</a></li>
				</ul>
				<p class="cc">Todos los derechos reservados © 2018 Leben Grupo Inmobiliario</p>
			</div>
		</div>
	</div>
</footer>

</body>
	<?php wp_footer() ?>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/plugins.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>
</html>